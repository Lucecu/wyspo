package es.lucecu.wyspo.ui.elementos;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import es.lucecu.wyspo.R;
import es.lucecu.wyspo.service.level.LevelItemVO;
import es.lucecu.wyspo.service.level.Movimientos;

public class LevelItemAdapter extends BaseAdapter {
    private Context mContext;

//    private static int filas = 3;
    private static int columnas;

    private static int posicion[];
    private static int posicionInicial[];

    private static LevelItemVO levelItems[];
    private static LevelItemVO levelItemsInicial[];

    // la posición es Y*columnas + X

    public LevelItemAdapter(Context c, int columnas, int posicionInicial[], LevelItemVO levelItems[]) {
        mContext = c;
        this.columnas = columnas;
//        this.filas = (int) Math.ceil(levelItems.length / columnas);
        this.posicionInicial = posicionInicial;
        this.posicion = posicionInicial.clone();
        this.levelItemsInicial = levelItems;
        this.levelItems = levelItemsInicial.clone();
    }

    public static boolean realizaMovimiento(int movimiento) {

        int posicionAnterior = posicion[1]*columnas + posicion[0];

        if(movimiento == Movimientos.MOVE_ONE_RIGHT) {
            posicion[0] += 1;
        } else if(movimiento == Movimientos.MOVE_ONE_LEFT) {
            posicion[0] -= 1;
        }

        int nuevaPosicion = posicion[1]*columnas + posicion[0];

        Log.i("ÑÑÑEEE",posicionAnterior + " - " + nuevaPosicion);

        levelItems[posicionAnterior] = new LevelItemVO(0); // TODO usar estados de levelIItem
        levelItems[nuevaPosicion] = new LevelItemVO(R.drawable.mbot); // TODO usar estados de levelIItem

        return true;
    }

    public int getColumnas() {
        return columnas;
    }

    /*
        métodos propios del Adapter
     */
    public int getCount() {
        return levelItems.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        LevelItemView imageView;
        // TODO crear sistema de estados
//        if (convertView == null) {
//            // if it's not recycled, initialize some attributes
            imageView = new LevelItemView(mContext, levelItems[position]);
//        } else {
//            imageView = (LevelItemView) convertView;
//        }
        return imageView;
    }
}
