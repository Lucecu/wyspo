package es.lucecu.wyspo.ui.elementos;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

public class PizarraBlancaLayout extends RelativeLayout {

    public PizarraBlancaLayout(@NonNull Context context) {
        super(context);
    }

    public PizarraBlancaLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

//    public PizarraBlancaLayout(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//    }
//
//    public PizarraBlancaLayout(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }

    @Override
    public void addView(View child) {
        super.addView(child);

    }
}
