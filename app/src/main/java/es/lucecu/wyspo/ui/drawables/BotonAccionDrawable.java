package es.lucecu.wyspo.ui.drawables;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.Log;

public class BotonAccionDrawable extends Drawable {

    public static final int BTN_ACTIVO = 1;
    public static final int BTN_REPOSO = 2;

    public static final int BTN_TIPO_BASE = 100;
    public static final int BTN_TIPO_IF = 101;

    private Paint paint;
    private int tipo = BTN_TIPO_BASE;

    public BotonAccionDrawable(int... opciones) {
        paint = new Paint();
        paint.setColor(Color.BLUE); // por defecto
        paint.setAntiAlias(true);

        for(int opcion : opciones) {
            if(opcion == BTN_ACTIVO) {
                paint.setColor(Color.YELLOW);
            } else if(opcion == BTN_REPOSO) {
                paint.setColor(Color.BLUE);
            } else if(opcion == BTN_TIPO_BASE) {
                tipo = BTN_TIPO_BASE;
            } else if(opcion == BTN_TIPO_IF) {
                tipo = BTN_TIPO_IF;
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {

        if(tipo == BTN_TIPO_BASE) {
            int width = getBounds().width();
            int height = getBounds().height();

            RectF rect = new RectF(21, 21, width - 21, height - 21);
            canvas.drawRoundRect(rect, 30, 30, paint);

            Paint pincel = new Paint();
            pincel.setColor(Color.parseColor("#ff99cc00"));
            pincel.setAntiAlias(true);
//        pincel.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
            canvas.drawCircle(width - 18, height / 2, 22, pincel);

            canvas.drawCircle(20, height / 2, 20, paint);

        } else if(tipo == BTN_TIPO_IF) {
            int width = getBounds().width();
            int height = getBounds().height();

//            RectF rect = new RectF(21, 21, 201, 51);
//            canvas.drawRoundRect(rect, 30, 30, paint);
//            rect = new RectF(21, 21, 101, height - 21);
//            canvas.drawRoundRect(rect, 30, 30, paint);
//            rect = new RectF(21, height - 51, width - 21, height - 21);
//            canvas.drawRoundRect(rect, 30, 30, paint);
//            rect = new RectF(width - 51, 21, width - 21, height - 21);
//            canvas.drawRoundRect(rect, 30, 30, paint);

            RectF rect = new RectF(21, 0, 201, 19);
            canvas.drawRoundRect(rect, 30, 30, paint);
            rect = new RectF(21, 0, 101, height);
            canvas.drawRoundRect(rect, 10, 10, paint);
            rect = new RectF(21, height - 19, width - 21, height);
            canvas.drawRoundRect(rect, 30, 30, paint);
            rect = new RectF(width - 51, height/2 - 51, width - 21, height);
            canvas.drawRoundRect(rect, 10, 10, paint);

            Paint pincel = new Paint();
            pincel.setColor(Color.parseColor("#ff99cc00"));
            pincel.setAntiAlias(true);
//        pincel.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
            canvas.drawCircle(104, height / 2, 22, pincel);
            canvas.drawCircle(width - 18, height / 2, 22, pincel);

            canvas.drawCircle(20, height / 2, 20, paint);

        }
    }

    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        paint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    public void cambiaColorFondo(int color) {
        paint.setColor(color);
    }
}