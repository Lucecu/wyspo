package es.lucecu.wyspo.ui.activitys;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import es.lucecu.wyspo.R;
import es.lucecu.wyspo.service.cvo.AccionCVO;
import es.lucecu.wyspo.ui.elementos.BotonAccion;
import es.lucecu.wyspo.ui.elementos.BotonIf;
import es.lucecu.wyspo.ui.elementos.BotonStart;

public class WhiteboardActivity extends Activity implements View.OnClickListener {

    private static Context contexto;
    private RelativeLayout frameLayout;
    private Button btnStart, btnAdelante, btnMediaVuelta, btnAtras, btnDerecha, btnIzquierda, btnAdelanteRapido, btnAtrasRapido, btnVueltaEnteraRapido, btnIf, btnPartyHard;

    public static final int SHOW_DISTANCE_CM = 1;
    private static final Handler whiteboardActivityHandler = new Handler(){
        public void handleMessage(Message msg) {
            if(msg.what == SHOW_DISTANCE_CM){
                Toast.makeText(contexto, msg.arg1 + " cm", Toast.LENGTH_LONG).show();
            }
        }
    };

    public static Handler getHandler(){
        return whiteboardActivityHandler;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whiteboard);

        contexto = this;

        frameLayout = (RelativeLayout) findViewById(R.id.whiteboard_frameLayout);

        btnStart = (Button) findViewById(R.id.whiteboard_btnStart);
        btnStart.setOnClickListener(this);

        btnAdelante = (Button) findViewById(R.id.whiteboard_btnAdelante);
        btnAdelante.setOnClickListener(this);

        btnMediaVuelta = (Button) findViewById(R.id.whiteboard_btnMediaVuelta);
        btnMediaVuelta.setOnClickListener(this);

        btnAtras = (Button) findViewById(R.id.whiteboard_btnAtras);
        btnAtras.setOnClickListener(this);

        btnDerecha = (Button) findViewById(R.id.whiteboard_btnDerecha);
        btnDerecha.setOnClickListener(this);

        btnIzquierda = (Button) findViewById(R.id.whiteboard_btnIzquierda);
        btnIzquierda.setOnClickListener(this);

        btnAdelanteRapido = (Button) findViewById(R.id.whiteboard_btnAdelanteRapido);
        btnAdelanteRapido.setOnClickListener(this);

        btnAtrasRapido = (Button) findViewById(R.id.whiteboard_btnAtrasRapido);
        btnAtrasRapido.setOnClickListener(this);

        btnVueltaEnteraRapido = (Button) findViewById(R.id.whiteboard_btnVueltaEnteraRapido);
        btnVueltaEnteraRapido.setOnClickListener(this);

        btnIf = (Button) findViewById(R.id.whiteboard_btnIf);
        btnIf.setOnClickListener(this);

        btnPartyHard = (Button) findViewById(R.id.whiteboard_btnPartyHard);
        btnPartyHard.setOnClickListener(this);

        Toast.makeText(this, String.valueOf(savedInstanceState), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        if (view == btnStart) {

            BotonStart bt = new BotonStart(this);
            bt.setText("GO");
            frameLayout.addView(bt);

        } else if (view == btnAdelante) {

            // adelante
            byte andar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x9c, (byte) 0xff, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x64, (byte) 0x00};
            // parar
            byte parar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x00, (byte) 0x00};
            AccionCVO adelante = new AccionCVO(andar, 1500, parar); // TODO sacame de aquí!!

            BotonAccion bt = new BotonAccion(this, adelante);
            bt.setText("-->");
            frameLayout.addView(bt);

        } else if (view == btnMediaVuelta) {

            // girar
            byte girar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x64, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x64, (byte) 0x00};
            // parar
            byte parar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x00, (byte) 0x00};
            AccionCVO adelante = new AccionCVO(girar, 1800, parar); // TODO sacame de aquí!!

            BotonAccion bt = new BotonAccion(this, adelante);
            bt.setText("<->");
            frameLayout.addView(bt);

        } else if (view == btnAtras) {

            // atras
            byte atras[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x9c, (byte) 0xff, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x64, (byte) 0x00};
            // parar
            byte parar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x00, (byte) 0x00};
            AccionCVO atrasBtn = new AccionCVO(atras, 1500, parar); // TODO sacame de aquí!!

            BotonAccion bt = new BotonAccion(this, atrasBtn);
            bt.setText("<--");
            frameLayout.addView(bt);

        } else if (view == btnDerecha) {

            // girar
            byte girar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x9c, (byte) 0xff, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x9c, (byte) 0xff};
            // parar
            byte parar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x00, (byte) 0x00};
            AccionCVO dcha = new AccionCVO(girar, 1000, parar); // TODO sacame de aquí!!

            BotonAccion bt = new BotonAccion(this, dcha);
            bt.setText("Dcha");
            frameLayout.addView(bt);

        } else if (view == btnIzquierda) {

            // girar
            byte girar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x64, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x64, (byte) 0x00};
            // parar
            byte parar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x00, (byte) 0x00};
            AccionCVO Izqd = new AccionCVO(girar, 1000, parar); // TODO sacame de aquí!!

            BotonAccion bt = new BotonAccion(this, Izqd);
            bt.setText("Izqd");
            frameLayout.addView(bt);

        } else if (view == btnAdelanteRapido) {

            // adelante
            byte andar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x01, (byte) 0xff, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0xff, (byte) 0x00};
            // parar
            byte parar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x00, (byte) 0x00};
            AccionCVO adelante = new AccionCVO(andar, 1500, parar); // TODO sacame de aquí!!

            BotonAccion bt = new BotonAccion(this, adelante);
            bt.setText("---->");
            frameLayout.addView(bt);

        } else if (view == btnAtrasRapido) {

            // adelante
            byte atras[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x01, (byte) 0xff, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0xff, (byte) 0x00};
            // parar
            byte parar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x00, (byte) 0x00};
            AccionCVO atrasBtn = new AccionCVO(atras, 1500, parar); // TODO sacame de aquí!!

            BotonAccion bt = new BotonAccion(this, atrasBtn);
            bt.setText("<----");
            frameLayout.addView(bt);

        } else if (view == btnVueltaEnteraRapido) {

            // girar
            byte girar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0xff, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0xff, (byte) 0x00};
            // parar
            byte parar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x00, (byte) 0x00};
            AccionCVO vueltaEntera = new AccionCVO(girar, 1500, parar); // TODO sacame de aquí!!

            BotonAccion bt = new BotonAccion(this, vueltaEntera);
            bt.setText("<<--->>");
            frameLayout.addView(bt);

        } else if (view == btnIf) {

            // ultrasonicsensor
            byte ultrasonic[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x04, (byte) 0x02, (byte) 0x01, (byte) 0x01, (byte) 0x03}; //ff 55 04 02 01 01 03
            AccionCVO ultrasonicSensor = new AccionCVO(ultrasonic, 1000); // TODO sacame de aquí!!

            BotonIf bt = new BotonIf(this, ultrasonicSensor);
            bt.setText("¿IF?");
            frameLayout.addView(bt);

        } else if (view == btnPartyHard) {

            // adelante
            byte andar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x9c, (byte) 0xff, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x64, (byte) 0x00};
            // parar
            byte parar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x00, (byte) 0x00};
            // derecha
            byte derecha[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x9c, (byte) 0xff, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x9c, (byte) 0xff};
            // izquierda
            byte izquierda[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x64, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x64, (byte) 0x00};


            // girar
            byte girar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0xff, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0xff, (byte) 0x00};

            AccionCVO AccAdelante100 = new AccionCVO(andar, 100, parar); // TODO sacame de aquí!!
            AccionCVO AccAdelante500 = new AccionCVO(andar, 500, parar); // TODO sacame de aquí!!
            AccionCVO AccAdelante1000 = new AccionCVO(andar, 1000, parar); // TODO sacame de aquí!!
            AccionCVO AccDerecha112 = new AccionCVO(derecha, 112, parar); // TODO sacame de aquí!!
            AccionCVO AccDerecha300 = new AccionCVO(derecha, 300, parar); // TODO sacame de aquí!!
            AccionCVO AccIzquierda112 = new AccionCVO(izquierda, 112, parar); // TODO sacame de aquí!!
            AccionCVO AccIzquierda300 = new AccionCVO(izquierda, 300, parar); // TODO sacame de aquí!!
            AccionCVO parar300 = new AccionCVO(parar, 300); // TODO sacame de aquí!!

            AccionCVO girar25000 = new AccionCVO(girar, 25000, parar);


            AccionCVO partyHard[] = {AccAdelante500, AccDerecha112, AccIzquierda112, parar300,
                                     AccAdelante100, AccAdelante100, AccAdelante100, AccAdelante500, AccDerecha112, AccIzquierda112, parar300,
                                     AccDerecha300, AccIzquierda300, AccAdelante500, AccDerecha112, AccIzquierda112};

            AccionCVO girarRapido[] = {girar25000};

            BotonAccion bt = new BotonAccion(this, partyHard);
            bt.setText("PARTY HARD");
            frameLayout.addView(bt);

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
