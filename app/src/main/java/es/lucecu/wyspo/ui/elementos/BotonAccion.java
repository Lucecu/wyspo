package es.lucecu.wyspo.ui.elementos;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import es.lucecu.wyspo.service.cvo.AccionCVO;
import es.lucecu.wyspo.service.listeners.BotonAccionTouchListener;
import es.lucecu.wyspo.ui.drawables.BotonAccionDrawable;

public class BotonAccion extends AppCompatButton{

    protected Context contexto;
    protected AccionCVO accion[];
    protected boolean estaActivo;

    protected int ancho = 200;
    protected int alto = 200;

    protected BotonAccion eslabonDerecho;
    protected BotonAccion eslabonIzquierdo;
    protected BotonAccion eslabonSuperior;
    protected BotonAccion eslabonInferior;

    protected BotonAccion(Context contexto) {
        super(contexto);
        this.contexto = contexto;
        this.accion = null;
        this.estaActivo= false;

        configurarBoton();
    }

    public BotonAccion(Context contexto, AccionCVO accion) {
        super(contexto);
        this.contexto = contexto;
        this.accion = new AccionCVO[]{accion};
        this.estaActivo= false;

        configurarBoton();
    }

    public BotonAccion(Context contexto, AccionCVO accion[]) {
        super(contexto);
        this.contexto = contexto;
        this.accion = accion;
        this.estaActivo= false;

        configurarBoton();
    }

    private void configurarBoton() {
        setId(View.generateViewId());

        BotonAccionDrawable drawable = new BotonAccionDrawable(BotonAccionDrawable.BTN_REPOSO);
        setBackground(drawable);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(120, 120);
        params.height = alto;
        params.width = ancho;
        setLayoutParams(params);

        setOnTouchListener(new BotonAccionTouchListener(contexto){
            @Override
            public void onClick(View view) {
                onSimpleClick(view);
            }
        });
    }

    public void activar() {
        this.estaActivo = true;
        BotonAccionDrawable drawable = new BotonAccionDrawable(BotonAccionDrawable.BTN_ACTIVO);
        setBackground(drawable);
    }

    public void desactivar() {
        this.estaActivo = false;
        BotonAccionDrawable drawable = new BotonAccionDrawable(BotonAccionDrawable.BTN_REPOSO);
        setBackground(drawable);
    }

    protected void onSimpleClick(View view) {
        // para sobreescribir en clases que hereden
    }

    public AccionCVO[] getAccion() {
        return accion;
    }

    public boolean estaActivo() {
        return estaActivo;
    }

    public BotonAccion getEslabonDerecho() {
        return eslabonDerecho;
    }

    public void setEslabonDerecho(BotonAccion eslabonDerecho) {
        this.eslabonDerecho = eslabonDerecho;
    }

    public BotonAccion getEslabonIzquierdo() {
        return eslabonIzquierdo;
    }

    public void setEslabonIzquierdo(BotonAccion eslabonIzquierdo) {
        this.eslabonIzquierdo = eslabonIzquierdo;
    }

    public BotonAccion getEslabonSuperior() {
        return eslabonSuperior;
    }

    public void setEslabonSuperior(BotonAccion eslabonSuperior) {
        this.eslabonSuperior = eslabonSuperior;
    }

    public BotonAccion getEslabonInferior() {
        return eslabonInferior;
    }

    public void setEslabonInferior(BotonAccion eslabonInferior) {
        this.eslabonInferior = eslabonInferior;
    }
}
