package es.lucecu.wyspo.ui.elementos;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.widget.ImageView;

import es.lucecu.wyspo.service.level.LevelItemVO;

public class LevelItemView extends AppCompatImageView {

    public LevelItemView(Context context, LevelItemVO levelItem) {
        super(context);

        setAdjustViewBounds(true);
        setScaleType(ImageView.ScaleType.FIT_CENTER);

        // TODO cambiar para poder tener imagenes no locales
        setImageResource(levelItem.getImagenLocalResource());
    }
}
