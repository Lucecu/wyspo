package es.lucecu.wyspo.ui.elementos;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import es.lucecu.wyspo.R;
import es.lucecu.wyspo.service.cvo.DispositivoCVO;

public class ItemDispositivoAdapter extends ArrayAdapter<DispositivoCVO> {

    // Lookup view for data population
    TextView tvNombre;
    TextView tvEstado;
    TextView tvMAC;

    public ItemDispositivoAdapter(Context context, ArrayList<DispositivoCVO> dispositivos) {
        super(context, 0, dispositivos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        DispositivoCVO dispositivo = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_dispositivo, parent, false);
        }

        // Lookup view for data population
        tvNombre = (TextView) convertView.findViewById(R.id.listDeviceName);
        tvEstado = (TextView) convertView.findViewById(R.id.listDeviceStatus);
        tvMAC = (TextView) convertView.findViewById(R.id.listDeviceMAC);

        // Populate the data into the template view using the data object
        tvNombre.setText(dispositivo.getNombre());
        tvMAC.setText(dispositivo.getMac());
        setTvEstado(dispositivo.getEstado());

        // Return the completed view to render on screen
        return convertView;
    }

    private void setTvEstado(int estado) {
        if(tvEstado != null) {
            if(estado == DispositivoCVO.ESTADO_OFFLINE) {
                tvEstado.setTextColor(Color.RED);
                tvEstado.setText("HARDCODED OFFLINE"); // TODO deshardcodear
            } else if(estado == DispositivoCVO.ESTADO_ONLINE) {
                tvEstado.setTextColor(Color.GREEN);
                tvEstado.setText("HARDCODED ONLINE"); // TODO deshardcodear
            } else if(estado == DispositivoCVO.ESTADO_CONECTANDO) {
                tvEstado.setTextColor(Color.YELLOW);
                tvEstado.setText("HARDCODED Conectando"); // TODO deshardcodear
            }
        }
    }
}
