package es.lucecu.wyspo.ui.activitys;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import es.lucecu.wyspo.R;
import es.lucecu.wyspo.service.cvo.AccionCVO;
import es.lucecu.wyspo.service.level.LevelAccionsEnum;
import es.lucecu.wyspo.service.level.LevelItemVO;
import es.lucecu.wyspo.ui.elementos.BotonAccion;
import es.lucecu.wyspo.ui.elementos.BotonStart;
import es.lucecu.wyspo.ui.elementos.LevelItemAdapter;

public class BaseLevelActivity extends Activity implements View.OnClickListener {

    private static Context contexto;
    private RelativeLayout frameLayout;
    private Button btnStart, btnAdelante,  btnAtras;
    private static LevelItemAdapter itemAdapter = null;

    private long levelId;

    public static final int MOVE_DONE = 1;
    private static final Handler dbaseLevelActivityHandler = new Handler(){
        public void handleMessage(Message msg) {
            if(msg.what == MOVE_DONE){
                itemAdapter.notifyDataSetChanged();
            }
        }
    };
    public static Handler getHandler(){
        return dbaseLevelActivityHandler;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_level);

        contexto = this;

        levelId = getIntent().getLongExtra("LEVEL_ID", 0L);
        GridView gridview = (GridView) findViewById(R.id.gridview);

        itemAdapter = generarNivel();
        gridview.setNumColumns(itemAdapter.getColumnas());
        gridview.setAdapter(itemAdapter);

//        gridview.setOnItemClickListener(new OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View v,
//                                    int position, long id) {
//                Toast.makeText(HelloGridView.this, "" + position,
//                        Toast.LENGTH_SHORT).show();
//            }
//        });


        // TODO importame o algo, esta replicado todo hacia abajo en whiteboardactivity
        frameLayout = (RelativeLayout) findViewById(R.id.whiteboard_frameLayout);

        btnStart = (Button) findViewById(R.id.whiteboard_btnStart);
        btnStart.setOnClickListener(this);

        btnAdelante = (Button) findViewById(R.id.whiteboard_btnAdelante);
        btnAdelante.setOnClickListener(this);

        btnAtras = (Button) findViewById(R.id.whiteboard_btnAtras);
        btnAtras.setOnClickListener(this);

        Toast.makeText(this, String.valueOf(savedInstanceState), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {

        // adelante
        byte andar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x9c, (byte) 0xff, (byte) '\n',
                (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x64, (byte) 0x00};
        // parar
        byte parar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) '\n',
                (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x00, (byte) 0x00};
        // girar
        byte girar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x64, (byte) 0x00, (byte) '\n',
                (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x64, (byte) 0x00};
        // girar
        byte girarInv[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x9c, (byte) 0xff, (byte) '\n',
                (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x9c, (byte) 0xff};
        // atras
        byte atras[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x9c, (byte) 0xff, (byte) '\n',
                (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x64, (byte) 0x00};

        if (view == btnStart) {

            BotonStart bt = new BotonStart(this);
            bt.setText("GO");
            frameLayout.addView(bt);

        } else if (view == btnAdelante) {

            AccionCVO adelante = new AccionCVO(andar, atras, 1500, parar, LevelAccionsEnum.X_PLUS_ONE); // TODO sacame de aquí!!

            BotonAccion bt = new BotonAccion(this, adelante);
            bt.setText("-->");
            frameLayout.addView(bt);

        } else if (view == btnAtras) {

            AccionCVO atrasBtn = new AccionCVO(atras, andar, 1500, parar, LevelAccionsEnum.X_LESS_ONE); // TODO sacame de aquí!!

            BotonAccion bt = new BotonAccion(this, atrasBtn);
            bt.setText("<--");
            frameLayout.addView(bt);

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }






    // TODO los niveles deben obtenerse desde bdd
    private LevelItemAdapter generarNivel() {

        LevelItemAdapter returnAdapter = null;

        if(levelId == 1) {
            // NIVEL 1 HARDCODED
            // TODO sacar a bdd
            int level1Columnas = 10;
            int posicionInicial[] = {1, 1};
            List<LevelItemVO> level1Items = new ArrayList<LevelItemVO>();

            long hardcodedLevelItemsId[] = {
                    HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_RIO, HARDCODED_RIO, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL,
                    HARDCODED_ARBOL, HARDCODED_MBOT, HARDCODED_VACIO, HARDCODED_MANZANA_OK, HARDCODED_ARBOL, HARDCODED_CASA, HARDCODED_RIO, HARDCODED_RIO, HARDCODED_ARBOL, HARDCODED_ARBOL,
                    HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_RIO, HARDCODED_RIO, HARDCODED_RIO
            };

            for (long id : hardcodedLevelItemsId) {
                level1Items.add(dameItemLevelVO(id));
            }

            returnAdapter = new LevelItemAdapter(this, level1Columnas, posicionInicial, level1Items.toArray(new LevelItemVO[level1Items.size()]));

        } else if(levelId == 2) {
            // NIVEL 2 HARDCODED
            // TODO sacar a bdd
            int level2Columnas = 16;
            int posicionInicial[] = {4, 1};
            List<LevelItemVO> level2Items = new ArrayList<LevelItemVO>();

            long hardcodedLevelItemsId[] = {
                    HARDCODED_CASA, HARDCODED_CASA, HARDCODED_CASA, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_RIO, HARDCODED_RIO, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_CASA, HARDCODED_CASA, HARDCODED_CASA,
                    HARDCODED_CASA, HARDCODED_CASA, HARDCODED_CASA, HARDCODED_ARBOL, HARDCODED_MBOT, HARDCODED_VACIO, HARDCODED_MANZANA_OK, HARDCODED_ARBOL, HARDCODED_CASA, HARDCODED_RIO, HARDCODED_RIO, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_CASA, HARDCODED_CASA, HARDCODED_CASA,
                    HARDCODED_CASA, HARDCODED_CASA, HARDCODED_CASA, HARDCODED_ARBOL, HARDCODED_VACIO, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_ARBOL, HARDCODED_RIO, HARDCODED_RIO, HARDCODED_RIO, HARDCODED_CASA, HARDCODED_CASA, HARDCODED_CASA
            };

            for (long id : hardcodedLevelItemsId) {
                level2Items.add(dameItemLevelVO(id));
            }

            returnAdapter = new LevelItemAdapter(this, level2Columnas, posicionInicial, level2Items.toArray(new LevelItemVO[level2Items.size()]));
        }

        return returnAdapter;
    }


    // TODO DESHARDCODEAR
    public final long HARDCODED_VACIO = 0;
    public final long HARDCODED_ARBOL = 1;
    public final long HARDCODED_RIO = 2;
    public final long HARDCODED_MBOT = 3;
    public final long HARDCODED_MANZANA_OK = 4;
    public final long HARDCODED_CASA = 5;

    private LevelItemVO dameItemLevelVO(long harcodedId) {

        // TODO sacar LevelItemVO de bdd basandose en el id recibido
        // TODO sacar estes LeveItemVO HARDCODEADOS
        LevelItemVO levelItem;
        if(harcodedId == HARDCODED_ARBOL) {
            levelItem = new LevelItemVO(R.drawable.arbol);
        } else if(harcodedId == HARDCODED_RIO) {
            levelItem = new LevelItemVO(R.drawable.rio);
        } else if(harcodedId == HARDCODED_MBOT) {
            levelItem = new LevelItemVO(R.drawable.mbot);
        } else if(harcodedId == HARDCODED_MANZANA_OK) {
            levelItem = new LevelItemVO(R.drawable.manzana_ok);
        } else if(harcodedId == HARDCODED_CASA) {
            levelItem = new LevelItemVO(R.drawable.casa);
        } else {
            levelItem = new LevelItemVO(0);
        }
        return levelItem;
    }
}
