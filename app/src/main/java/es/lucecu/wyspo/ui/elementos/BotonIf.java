package es.lucecu.wyspo.ui.elementos;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import es.lucecu.wyspo.service.asyncTasks.EjecutaAccionAsyncTask;
import es.lucecu.wyspo.service.cvo.AccionCVO;
import es.lucecu.wyspo.ui.drawables.BotonAccionDrawable;

public class BotonIf extends BotonAccion {

    public BotonIf(Context contexto, AccionCVO accion) {
        super(contexto, accion);
        ancho *= 1.7;

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(alto, ancho);
        params.height = alto;
        params.width = ancho;
        setLayoutParams(params);

        BotonAccionDrawable drawable = new BotonAccionDrawable(BotonAccionDrawable.BTN_REPOSO, BotonAccionDrawable.BTN_TIPO_IF);
        setBackground(drawable);
    }

    @Override
    protected void onSimpleClick(View view) {

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
        ancho += 200;
        params.width = ancho;
        setLayoutParams(params);

        Toast.makeText(contexto, "BTN IF", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void activar() {
        this.estaActivo = true;
        BotonAccionDrawable drawable = new BotonAccionDrawable(BotonAccionDrawable.BTN_ACTIVO, BotonAccionDrawable.BTN_TIPO_IF);
        setBackground(drawable);
    }

    @Override
    public void desactivar() {
        this.estaActivo = false;
        BotonAccionDrawable drawable = new BotonAccionDrawable(BotonAccionDrawable.BTN_REPOSO, BotonAccionDrawable.BTN_TIPO_IF);
        setBackground(drawable);
    }
}