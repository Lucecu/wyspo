package es.lucecu.wyspo.ui.elementos;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import es.lucecu.wyspo.service.asyncTasks.EjecutaAccionAsyncTask;

public class BotonStart extends BotonAccion {

    private LevelItemAdapter itemAdapter;

    public BotonStart(Context contexto) {
        super(contexto);
    }

    @Override
    protected void onSimpleClick(View view) {

        if(eslabonDerecho != null) {
            new EjecutaAccionAsyncTask(contexto).execute(eslabonDerecho);
        } else {
            Toast.makeText(contexto, " HARDCODED no hay eslabonDerecho", Toast.LENGTH_SHORT).show();
        }
    }
}