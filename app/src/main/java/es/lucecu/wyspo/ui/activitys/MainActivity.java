package es.lucecu.wyspo.ui.activitys;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;

import es.lucecu.wyspo.R;
import es.lucecu.wyspo.service.cvo.AccionCVO;
import es.lucecu.wyspo.ui.dialogos.DialogoBluetooth;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private int anchoPantalla,altoPantalla;
    private PopupWindow dialogoBluetooth;
    private FloatingActionButton btn_bluetooth;
    private Button btn_whiteboard, btn_base_level_1, btn_base_level_2, btn_test1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // TODO solicitar permisos de location si no los tiene concedidos
        // TODO revisar api level, revisar aviso previo, no nos interesa la location, android 6+ la requiere para ver los dispositivos bluetooth
        //this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);

        WindowManager window = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        anchoPantalla = window.getDefaultDisplay().getWidth();
        altoPantalla = window.getDefaultDisplay().getHeight();

        btn_bluetooth = (FloatingActionButton) findViewById(R.id.btn_bluetooth);
        btn_bluetooth.setOnClickListener(this);

        btn_whiteboard = (Button) findViewById(R.id.btn_whiteboard);
        btn_whiteboard.setOnClickListener(this);

        btn_base_level_1 = (Button) findViewById(R.id.btn_base_level_1);
        btn_base_level_1.setOnClickListener(this);

        btn_base_level_2 = (Button) findViewById(R.id.btn_base_level_2);
        btn_base_level_2.setOnClickListener(this);

        btn_test1 = (Button) findViewById(R.id.btn_test1);
        btn_test1.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if(view == btn_bluetooth) {
            dialogoBluetooth = new DialogoBluetooth(getApplicationContext(),(int)(anchoPantalla*0.75), (int)(altoPantalla*0.6));
            dialogoBluetooth.showAtLocation(findViewById(R.id.contenido), Gravity.CENTER, 0, 0);
        } else if (view == btn_test1) {
            // adelante
            byte andar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x9c, (byte) 0xff, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x64, (byte) 0x00};
            // girar
            byte girar[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x64, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x64, (byte) 0x00};
            // parar
            byte detener[] = new byte[]{(byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) '\n',
                    (byte) 0xff, (byte) 0x55, (byte) 0x06, (byte) 0x00, (byte) 0x02, (byte) 0x0a, (byte) 0x0a, (byte) 0x00, (byte) 0x00};

            AccionCVO adelante = new AccionCVO(andar, 1500, detener);

            AccionCVO mediaVuelta = new AccionCVO(girar, 1800, detener);

//            new EjecutaAccionAsyncTask(getApplicationContext()).execute(adelante, mediaVuelta, adelante, mediaVuelta);
        } else if(view == btn_whiteboard) {
            Intent whiteboardIntent = new Intent(this, WhiteboardActivity.class);
            startActivity(whiteboardIntent);
        } else if(view == btn_base_level_1) {
            Intent baseLevelIntent = new Intent(this, BaseLevelActivity.class);
            baseLevelIntent.putExtra("LEVEL_ID", 1L);
            startActivity(baseLevelIntent);
        } else if(view == btn_base_level_2) {
            Intent baseLevelIntent = new Intent(this, BaseLevelActivity.class);
            baseLevelIntent.putExtra("LEVEL_ID", 2L);
            startActivity(baseLevelIntent);
        }
    }
}
