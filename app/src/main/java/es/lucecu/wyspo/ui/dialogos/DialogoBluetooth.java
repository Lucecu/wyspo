package es.lucecu.wyspo.ui.dialogos;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import java.util.ArrayList;

import es.lucecu.wyspo.R;
import es.lucecu.wyspo.service.acciones.AccionService;
import es.lucecu.wyspo.service.cvo.DispositivoCVO;
import es.lucecu.wyspo.ui.elementos.ItemDispositivoAdapter;

public class DialogoBluetooth extends PopupWindow implements View.OnClickListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener{
    private Context contexto;

    private Button btnRefresh;
    private Button btnCerrar;

    private ListView listaDispositivosBluetooth;
    private static ItemDispositivoAdapter dispositivosAdapter;
    private final BluetoothAdapter BLUETOOTH_ADAPTER = BluetoothAdapter.getDefaultAdapter();

    private BroadcastReceiver recibidorBluetooth;

    public static final int UPDATE_LISTVIEW_CHANGES = 1;
    private static final Handler dialogoBluetoothHandler = new Handler(){
        public void handleMessage(Message msg) {
            if(msg.what == UPDATE_LISTVIEW_CHANGES){
                dispositivosAdapter.notifyDataSetChanged();
            }
        }
    };

    public static Handler getHandler(){
        return dialogoBluetoothHandler;
    }

    public DialogoBluetooth(Context ctx, int ancho, int alto){
        super(ctx);

        this.contexto = ctx;

        LinearLayout layout = (LinearLayout) LayoutInflater.from(contexto).inflate(R.layout.dialogo_bluetooth, new LinearLayout(contexto));
        setWidth(ancho);
        setHeight(alto);
        setOutsideTouchable(true);
        setFocusable(true);
        setContentView(layout);
        setOnDismissListener(this);

        listaDispositivosBluetooth = (ListView) layout.findViewById(R.id.listaDispositivosBluetooth);

        dispositivosAdapter = new ItemDispositivoAdapter(contexto, new ArrayList<DispositivoCVO>());
        listaDispositivosBluetooth.setAdapter(dispositivosAdapter);

        btnRefresh = (Button) layout.findViewById(R.id.btn_refrescaBluetooth);
        btnCerrar = (Button) layout.findViewById(R.id.btn_cierraDialogoBluetooth);

        btnRefresh.setOnClickListener(this);
        btnCerrar.setOnClickListener(this);
        listaDispositivosBluetooth.setOnItemClickListener(this);

        // Create a BroadcastReceiver for ACTION_FOUND
        recibidorBluetooth = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                // When discovery finds a device
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    // Get the BluetoothDevice object from the Intent
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    // Add the name and address to an array adapter to show in a ListView
                    // compruebo que no está ya añadido antes
                    boolean dispositivoNuevo = true;
                    for(int i = 0 ; i < dispositivosAdapter.getCount() ; i++) {
                        if(dispositivosAdapter.getItem(i).contieneDispositivo(device)) {
                            dispositivoNuevo = false;
                        }
                    }
                    if(dispositivoNuevo) {
                        dispositivosAdapter.add(new DispositivoCVO(device));
                    }
                }
            }
        };

        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        contexto.registerReceiver(recibidorBluetooth, filter); // TODO Don't forget to unregister during onDestroy

        // al abrir el dialogo, fuerzo el startDiscovery
        // si ya hay algun dispositivo activo, lo añado antes a la lista:
        if(AccionService.hayDispositivosActivos()) {
            dispositivosAdapter.add(AccionService.getDispositivoActivo());
        }
        BLUETOOTH_ADAPTER.startDiscovery();
    }

    @Override
    public void onDismiss() {
        // TODO enviar a ActivityHandler que ponga el alpha a 0
//        contexto.getForeground().setAlpha(0);
        contexto.unregisterReceiver(recibidorBluetooth);
        if (BLUETOOTH_ADAPTER.isDiscovering()){
            BLUETOOTH_ADAPTER.cancelDiscovery();
        }
    }

    @Override
    public void onClick(View view) {
        if(view == btnRefresh) {
            Log.d("ÑÑÑEEE","pulso btn 1");
            if (!contexto.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
                Toast.makeText(contexto, "bluetooth LE", Toast.LENGTH_SHORT).show();
            } else {
                if (BLUETOOTH_ADAPTER != null) {
                    //if (!BLUETOOTH_ADAPTER.isEnabled()) {
                        //Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        //startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                    //}

                    //cancel any prior BT device discovery
                    if (BLUETOOTH_ADAPTER.isDiscovering()){
                        BLUETOOTH_ADAPTER.cancelDiscovery();
                    }

                    //re-start discovery
                    dispositivosAdapter.clear();
                    // si ya hay alguno activo, lo añado antes a la lista:
                    if(AccionService.hayDispositivosActivos()) {
                        dispositivosAdapter.add(AccionService.getDispositivoActivo());
                    }
                    BLUETOOTH_ADAPTER.startDiscovery();
                } else {
                    Toast.makeText(contexto, "HARDCODED bluetoothAdapter falló", Toast.LENGTH_SHORT).show();
                }

            }
        } else if(view == btnCerrar) {
            if(AccionService.hayDispositivosActivos()) {
                try {
//                    byte data[] = AccionService.dameAccion(10, 1, 1, 100);
//                    String hexStr = "";
//                    for(int i1=0;i1<data.length;i1++){
//                        hexStr+=String.format("%02X ", data[i1]);
//                    }
//                    Toast.makeText(contexto, hexStr, Toast.LENGTH_LONG).show();


                    // try1
                    {

//                        // andar hacia alante
//                        bluetoothThread.getBluetoothSocket().getOutputStream().write(testBORRAME(10, 9, 1, -100));
//                        bluetoothThread.getBluetoothSocket().getOutputStream().write(testBORRAME(10, 10, 1, 100));
//                        Thread.sleep(1500);
//                        // media vuelta
//                        bluetoothThread.getBluetoothSocket().getOutputStream().write(testBORRAME(10, 9, 1, 100));
//                        bluetoothThread.getBluetoothSocket().getOutputStream().write(testBORRAME(10, 10, 1, 100));
//                        Thread.sleep(2000);
//                        // volver
//                        bluetoothThread.getBluetoothSocket().getOutputStream().write(testBORRAME(10, 9, 1, -100));
//                        bluetoothThread.getBluetoothSocket().getOutputStream().write(testBORRAME(10, 10, 1, 100));
//                        Thread.sleep(1500);
//                        bluetoothThread.getBluetoothSocket().getOutputStream().write(testBORRAME(10, 9, 1, 0));
//                        bluetoothThread.getBluetoothSocket().getOutputStream().write(testBORRAME(10, 10, 1, 0));
                    }

                    Toast.makeText(contexto, "WIKI?", Toast.LENGTH_LONG).show();

                }catch(Exception e){
                    Toast.makeText(contexto, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(contexto, "es null", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int posicion, long id) {

        DispositivoCVO dispositivo = (DispositivoCVO)parent.getItemAtPosition(posicion);

        // si no estoy conectado a nadie, me conecto
        if(!AccionService.hayDispositivosActivos()) {
            dispositivo.iniciaConexion();
            AccionService.addDispositivoActivo(dispositivo);
            dispositivosAdapter.notifyDataSetChanged();

        // si estoy conectado a alguien, miro si se ha hecho click en el mismo
        // si es el mismo, lo desconecto
        } else if(AccionService.getDispositivoActivo() == dispositivo) {
            desconectarDispositivoActivo();

        // si es otro, saco un toast y no hago nada
        } else {
            Toast.makeText(contexto, "HARDCODED ya estoy conectado a otro dispositivo", Toast.LENGTH_SHORT).show();
        }

//handleer, si conectando falla
//        dispositivoActivo = null;
//        bluetoothThread.closeConnection();
//        bluetoothThread = null;
//handler, si conectando es exito
//        dispositivoActivo.setEstado(DispositivoCVO.ESTADO_ONLINE);
//        actualizaInterfaz;
    }

    private void desconectarDispositivoActivo() {
        AccionService.getDispositivoActivo().finalizaConexion();
        AccionService.removeDispositivoActivo(AccionService.getDispositivoActivo()); // tiene sentido cambiar el estado antes ya que está referenciado por la lista
        dispositivosAdapter.notifyDataSetChanged();
    }
}
