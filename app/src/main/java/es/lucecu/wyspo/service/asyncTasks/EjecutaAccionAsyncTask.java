package es.lucecu.wyspo.service.asyncTasks;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import es.lucecu.wyspo.service.acciones.AccionService;
import es.lucecu.wyspo.service.cvo.AccionCVO;
import es.lucecu.wyspo.service.level.LevelAccionsEnum;
import es.lucecu.wyspo.ui.activitys.BaseLevelActivity;
import es.lucecu.wyspo.ui.activitys.WhiteboardActivity;
import es.lucecu.wyspo.ui.dialogos.DialogoBluetooth;
import es.lucecu.wyspo.ui.elementos.BotonAccion;
import es.lucecu.wyspo.ui.elementos.BotonIf;
import es.lucecu.wyspo.ui.elementos.BotonStart;
import es.lucecu.wyspo.ui.elementos.LevelItemAdapter;

public class EjecutaAccionAsyncTask extends AsyncTask<View, View, Long> {

    private static final int NO_HAY_DISPOSITIVOS_ACTIVOS = 101;
    private static final int EL_BOTON_NO_ES_INICIAL = 102;
    private static final int HA_FALLADO_LA_COMUNICACION = 103;

    private Context context;
    private int codError = 0;

    public EjecutaAccionAsyncTask(Context contexto) {
        this.context = contexto;
    }

    protected Long doInBackground(View... botones) {

//        if(AccionService.hayDispositivosActivos()) {
            View botonInicial = botones[0];

            if(botonInicial instanceof BotonAccion) {
                if(ejecutaAccionesRecursivas((BotonAccion)botonInicial)) {
                    // funciono ok
                } else {
                    codError = HA_FALLADO_LA_COMUNICACION;
                }
            } else {
                codError = EL_BOTON_NO_ES_INICIAL;
            }
//        } else {
//            codError = NO_HAY_DISPOSITIVOS_ACTIVOS;
//        }

        return 0l;
    }

    protected void onProgressUpdate(View... botones) {
        BotonAccion botonEnCurso = (BotonAccion) botones[0];

        if(botonEnCurso.estaActivo()) {
            // lo desactivo
            botonEnCurso.desactivar();
        } else {
            // lo activo
            botonEnCurso.activar();
        }
    }

    protected void onPostExecute(Long result) {
        if(codError == NO_HAY_DISPOSITIVOS_ACTIVOS) {
            Toast.makeText(context, "HARDCODED: No hay ningún dispositivo activo.", Toast.LENGTH_LONG).show();
        } else if(codError == EL_BOTON_NO_ES_INICIAL) {
            Toast.makeText(context, "HARDCODED: No es un botón inicial.", Toast.LENGTH_LONG).show();
        } else if(codError == HA_FALLADO_LA_COMUNICACION) {
            Toast.makeText(context, "HARDCODED: Ha fallado la comunicación.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "onPostExecute", Toast.LENGTH_LONG).show();
        }
    }

    private boolean ejecutaAccionesRecursivas(BotonAccion boton) {
        boolean returnValue = false;
        byte[] respuestaAccion = null;

        try {
            publishProgress(boton);

            AccionCVO acciones[] = boton.getAccion();
            for(AccionCVO accion : acciones) {
                if (accion != null) {


                    if (accion.getLevelMove() != null) {
//                    LevelAccionsEnum levelMove = accion.getLevelMove();
                        LevelItemAdapter.realizaMovimiento(accion.getLevelMove().getMovimiento());
                        Message msg = new Message();
                        msg.what = BaseLevelActivity.MOVE_DONE;
                        BaseLevelActivity.getHandler().sendMessage(msg);
                    }


                    if (AccionService.hayDispositivosActivos()) {
                        respuestaAccion = AccionService.getDispositivoActivo().ejecutaAccion(accion.getComando());
                    }

                    Thread.sleep(accion.getDuracion());
                    if (accion.getComandoPOST() != null && AccionService.hayDispositivosActivos()) {
                        AccionService.getDispositivoActivo().ejecutaAccion(accion.getComandoPOST());
                    }
                    Thread.sleep(100); // separación entre botones
                }
            }


            // TODO sacar de aquí para que pase a formar parte de una accion
            // si es un if, leo la respuesta de la acción para saber que hacer
            if(boton.getClass().equals(BotonIf.class)) {
                // byte es signed, con el 0xff se  recupera el valor del último byte
                int distancia = ((respuestaAccion[7]&0xff) << 24) + ((respuestaAccion[6]&0xff) << 16) + ((respuestaAccion[5]&0xff) << 8) + (respuestaAccion[4]&0xff);

                double centimetros = Math.floor(Float.intBitsToFloat(distancia)*10.0)/10.0; // Convertir en CM

                Message msg = new Message();
                msg.what = WhiteboardActivity.SHOW_DISTANCE_CM;
                msg.arg1 = (int) centimetros;
                WhiteboardActivity.getHandler().sendMessage(msg);

Log.i("ÑÑÑEEE012", centimetros + " cm");
                if(centimetros < 15) {
                    if (boton.getEslabonSuperior() != null) {
                        ejecutaAccionesRecursivas(boton.getEslabonSuperior());
                    }
                }
            }



            publishProgress(boton);
            if (boton.getEslabonDerecho() != null) {
                ejecutaAccionesRecursivas(boton.getEslabonDerecho());
            }
            returnValue = true;
        }catch(Exception e) {
            e.printStackTrace();
            try {
                AccionService.getDispositivoActivo().finalizaConexion();
            }catch(Exception ex){}
        }

        return returnValue;
    }
}
