package es.lucecu.wyspo.service.level;

public enum LevelAccionsEnum {

    X_PLUS_ONE(Movimientos.MOVE_ONE_RIGHT, Movimientos.MOVE_ONE_LEFT),
    X_LESS_ONE(Movimientos.MOVE_ONE_LEFT, Movimientos.MOVE_ONE_RIGHT);

    private final int movimiento;
    private final int vueltaAtras;

    LevelAccionsEnum(int movimiento, int vueltaAtras) {
        this.movimiento = movimiento;
        this.vueltaAtras = vueltaAtras;
    }

    public int getMovimiento() { return movimiento; }

    public int getVueltaAtras() { return vueltaAtras; }
}
