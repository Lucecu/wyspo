package es.lucecu.wyspo.service.level;

public class LevelItemVO {

    private int imagenLocal;
    private String rutaImagen;

    public LevelItemVO(int imagenLocal) {
        this.imagenLocal = imagenLocal;
    }

    public LevelItemVO(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }

    public int getImagenLocalResource() {
        return imagenLocal;
    }


}
