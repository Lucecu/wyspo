package es.lucecu.wyspo.service.asyncTasks;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import es.lucecu.wyspo.service.cvo.DispositivoCVO;
import es.lucecu.wyspo.ui.elementos.BotonAccion;

public class BluetoothAsyncTask extends AsyncTask<DispositivoCVO, Void, Long> {

    private static final UUID mBot_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static Map<BluetoothDevice, BluetoothSocket> sockets = new HashMap<BluetoothDevice, BluetoothSocket>();

    protected Long doInBackground(DispositivoCVO... dispositivos) {

        DispositivoCVO dispositivo = dispositivos[0];
        BluetoothSocket socket = null;

        dispositivo.setEstado(DispositivoCVO.ESTADO_CONECTANDO);

        // Cancel discovery because it will slow down the connection
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothAdapter.cancelDiscovery();

        // Get a BluetoothSocket to connect with the given BluetoothDevice
        try {
            // TODO emparejar sin pin
            BluetoothDevice device = dispositivo.getDevice();
            socket = device.createRfcommSocketToServiceRecord(mBot_UUID);

            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
            socket.connect();

            sockets.put(device, socket);
            dispositivo.setEstado(DispositivoCVO.ESTADO_ONLINE);
        } catch (Exception connectException) {
            // Unable to connect; close the socket and get out
            try {
                closeConnection(dispositivo);
            } catch (Exception closeException) { }
            dispositivo.setEstado(DispositivoCVO.ESTADO_ERROR);
        }

        // Do work to manage the connection (in a separate thread)
        //manageConnectedSocket(mmSocket);
        return 0l;
    }

    protected void onPostExecute(long result) {

    }

    /** Will cancel an in-progress connection, and close the socket */
    public void closeConnection(DispositivoCVO dispositivo) {
        try {
            BluetoothSocket socket = sockets.get(dispositivo.getDevice());
            socket.close();
        } catch (Exception e) { }
        sockets.remove(dispositivo.getDevice());
        dispositivo.setEstado(DispositivoCVO.ESTADO_OFFLINE);
    }

    public BluetoothSocket getBluetoothSocket(BluetoothDevice device) {
        return sockets.get(device);
    }
}