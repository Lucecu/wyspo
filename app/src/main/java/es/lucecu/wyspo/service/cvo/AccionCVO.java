package es.lucecu.wyspo.service.cvo;

import es.lucecu.wyspo.service.level.LevelAccionsEnum;

public class AccionCVO {
    private byte comando[];
    private byte comandoInverso[];
    private LevelAccionsEnum levelMove;
    private long duracion;
    private byte comandoPOST[];

    public AccionCVO(byte[] comando) {
        this.comando = comando;
        this.comandoInverso = null;
        this.levelMove = null;
        this.duracion = 0;
        this.comandoPOST = null;
    }

    public AccionCVO(byte[] comando, byte[] comandoInverso) {
        this(comando);
        this.comandoInverso = comandoInverso;
    }

    public AccionCVO(byte[] comando, long duracionMillis) {
        this(comando);
        this.duracion = duracionMillis;
    }

    public AccionCVO(byte[] comando, byte[] comandoInverso, long duracionMillis) {
        this(comando, comandoInverso);
        this.duracion = duracionMillis;
    }

    public AccionCVO(byte[] comando, long duracionMillis, byte[] comandoPOST) {
        this(comando, duracionMillis);
        this.comandoPOST = comandoPOST;
    }

    public AccionCVO(byte[] comando, byte[] comandoInverso, long duracionMillis, byte[] comandoPOST) {
        this(comando, comandoInverso, duracionMillis);
        this.comandoPOST = comandoPOST;
    }

    public AccionCVO(byte[] comando, byte[] comandoInverso, long duracionMillis, LevelAccionsEnum levelMove) {
        this(comando, comandoInverso, duracionMillis);
        this.levelMove = levelMove;
    }

    public AccionCVO(byte[] comando, byte[] comandoInverso, long duracionMillis, byte[] comandoPOST, LevelAccionsEnum levelMove) {
        this(comando, comandoInverso, duracionMillis, comandoPOST);
        this.levelMove = levelMove;
    }

    public byte[] getComando() {
        return comando;
    }

    public byte[] getComandoInverso() {
        return comandoInverso;
    }

    public LevelAccionsEnum getLevelMove() {
        return levelMove;
    }

    public long getDuracion() {
        return duracion;
    }

    public byte[] getComandoPOST() {
        return comandoPOST;
    }
}
