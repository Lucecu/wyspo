package es.lucecu.wyspo.service.acciones;

import java.nio.ByteBuffer;

import es.lucecu.wyspo.service.cvo.DispositivoCVO;

public class AccionService {

    private static DispositivoCVO dispositivoActivo = null;

    public static byte[] dameAccion(int type, int port, int slot, int value) {
        byte[] cmd = new byte[13];
		/*
		ff 55 09 00 02 08 07 02 [led] [Red] [Green] [Blue]
		ff 55 07 00 02 22 7b 00 fa 00

		ff 55 len idx action device port  slot  data a
		0  1  2   3   4      5      6     7     8
		*/
        //unsigned char a[11]={0xff,0x55,WRITEMODULE,7,0,0,0,0,0,0,'\n'};
        //a[4] = [type intValue];
        //a[5] = (port<<4 & 0xf0)|(slot & 0xf);
        cmd[0]=(byte) 0xff;
        cmd[1]=(byte) 0x55;
        cmd[2]=(byte) 9;
        cmd[3]=(byte) 0;
        cmd[4]=(byte) 2; //WRITEMODULE
        cmd[5]=(byte) type;
        cmd[6]=(byte) (port&0xff);
        cmd[7]=(byte) (slot&0xff);
        if(type==8){ // led
            cmd[7] = (byte) (0);
            cmd[8] = (byte) ((value>>8) & 0xff);
            cmd[9] = (byte) ((value>>16) & 0xff);
            cmd[10] = (byte) ((value>>24) & 0xff);
        }else if(type==10){ // motor
            final ByteBuffer buf = ByteBuffer.allocate(2);
            buf.putShort((short)value);
            buf.position(0);
            // Read back bytes
            final byte b1 = buf.get();
            final byte b2 = buf.get();
            cmd[8] = b1;
            cmd[9] = b2;

        }else if(type==11){
            cmd[8] = (byte) (value & 0xff);
        }else{
            float f = (float)value;
            int fi = Float.floatToIntBits(f);
            cmd[8] = (byte) (fi & 0xff);
            cmd[9] = (byte) ((fi>>8) & 0xff);
            cmd[10] = (byte) ((fi>>16) & 0xff);
            cmd[11] = (byte) ((fi>>24) & 0xff);
        }

        cmd[12]=(byte) '\n';
        return cmd;
    }

    public static boolean hayDispositivosActivos() {
        boolean returnValue = false;
        if(dispositivoActivo != null) {
            returnValue = true;
        }
        return returnValue;
    }

    public static void addDispositivoActivo(DispositivoCVO device) {
        AccionService.dispositivoActivo = device;
    }

    public static boolean removeDispositivoActivo(DispositivoCVO device) {
        dispositivoActivo = null;
        return true;
    }

    public static DispositivoCVO getDispositivoActivo() {
        return dispositivoActivo;
    }
}
