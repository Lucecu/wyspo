package es.lucecu.wyspo.service.listeners;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import es.lucecu.wyspo.ui.elementos.BotonAccion;
import es.lucecu.wyspo.ui.elementos.BotonIf;
import es.lucecu.wyspo.ui.elementos.PizarraBlancaLayout;

public class BotonAccionTouchListener implements View.OnTouchListener {

    private GestureDetector gestureDetector;

    private static final int MARGEN_ENGANCHE = 25;
    private static final int ANCHO_BTN_IF = 100;

    public BotonAccionTouchListener(Context contexto) {
        gestureDetector = new GestureDetector(contexto, new SingleTapConfirm());
    }

    private int _xDelta;
    private int _yDelta;

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        if (gestureDetector.onTouchEvent(event)) {
            // single tap
            onClick(view);
        } else {
            final int X = (int) event.getRawX();
            final int Y = (int) event.getRawY();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
//                    RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
//                    _xDelta = X - lParams.leftMargin;
//                    _yDelta = Y - lParams.topMargin;
                    _xDelta = X - view.getLeft();
                    _yDelta = Y - view.getTop();

                    // FIXME meter en view.sendToTop()??
                    PizarraBlancaLayout layout = (PizarraBlancaLayout) view.getParent();
                    layout.removeView(view);
                    layout.addView(view);


                    break;
                case MotionEvent.ACTION_UP:
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    break;
                case MotionEvent.ACTION_MOVE:

                    RelativeLayout.LayoutParams layoutParams = compruebaEslabones(view, X - _xDelta, Y - _yDelta);
                    if (layoutParams == null) {
                        layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();

                        BotonAccion botonEnganchado = ((BotonAccion) view).getEslabonIzquierdo();
                        if(botonEnganchado != null) {
                            // compruebo cual desengancho
                            if(view.equals(botonEnganchado.getEslabonDerecho())) {
                                botonEnganchado.setEslabonDerecho(null);
                                layoutParams.removeRule(RelativeLayout.RIGHT_OF);
                                layoutParams.removeRule(RelativeLayout.ALIGN_TOP);
                            } else if(view.equals(botonEnganchado.getEslabonSuperior())) {
                                botonEnganchado.setEslabonSuperior(null);
                                layoutParams.removeRule(RelativeLayout.ALIGN_LEFT);
                                layoutParams.removeRule(RelativeLayout.ALIGN_TOP);
                            }

                            ((BotonAccion) view).setEslabonIzquierdo(null);
                        }
                        layoutParams.leftMargin = X - _xDelta;
                        layoutParams.topMargin = Y - _yDelta;
                    }
                    view.setLayoutParams(layoutParams);
                    break;
            }

            view.getRootView().invalidate();
        }

        return true;
    }

    private RelativeLayout.LayoutParams compruebaEslabones(View v, int x, int y) {

        BotonAccion seleccionado = (BotonAccion) v;
        if(seleccionado.getEslabonIzquierdo() == null) { // si no está enganchado
            PizarraBlancaLayout padre = (PizarraBlancaLayout) v.getParent();
            // TODO mejorar rendimiento
            for (int i = 0; i < padre.getChildCount(); i++) {
                BotonAccion boton = (BotonAccion) padre.getChildAt(i);

                if(boton.getClass() == BotonIf.class && boton.getEslabonSuperior() == null) {
                    if(compruebaDistanciasEngancheSuperior(boton, x, y)) {
                        Log.i("ÑEEE", "------- SE ENGANCHA ARRIBA -------");
                        ((BotonAccion) v).setEslabonIzquierdo(boton);
                        boton.setEslabonSuperior((BotonAccion) v);

                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
                        layoutParams.addRule(RelativeLayout.ALIGN_LEFT, boton.getId());
                        layoutParams.addRule(RelativeLayout.ALIGN_TOP, boton.getId());
                        layoutParams.leftMargin = 0 + 84;
                        layoutParams.topMargin = 0;
                        return layoutParams;
                    }
                }

                if (boton.getEslabonDerecho() == null && !boton.equals(v)) {
                    if(compruebaDistanciasEngancheDerecho(boton, x, y)) {
                        Log.i("ÑEEE", "------- SE ENGANCHA DERECHA -------");
                        ((BotonAccion) v).setEslabonIzquierdo(boton);
                        boton.setEslabonDerecho((BotonAccion) v);

                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
                        layoutParams.addRule(RelativeLayout.RIGHT_OF, boton.getId());
                        layoutParams.addRule(RelativeLayout.ALIGN_TOP, boton.getId());
                        layoutParams.leftMargin = 0 - 38;
                        layoutParams.topMargin = 0;
                        return layoutParams;
                    }
                }
            }

        } else { // si ya está enganchado
            // TODO revisar cuando se desengancha
            BotonAccion botonEnganchado = seleccionado.getEslabonIzquierdo();

            // si aún estamos en zona de enganche, devolvemos botonEnganchado
            // sino, se vuelve null para que el botón se suelte
            if(compruebaDistanciasEngancheDerecho(botonEnganchado, x, y)) {
                Log.i("ÑEEE", "------- SIGUE ENGANCHADO DERECHA -------");
                return (RelativeLayout.LayoutParams) v.getLayoutParams();
            } else if(compruebaDistanciasEngancheSuperior(botonEnganchado, x, y)) {
                Log.i("ÑEEE", "------- SIGUE ENGANCHADO ARRIBA -------");
                return (RelativeLayout.LayoutParams) v.getLayoutParams();
            }
            Log.i("ÑEEE", "------- SE SUELTA -------");
        }

        return null;
    }

    private boolean compruebaDistanciasEngancheDerecho(BotonAccion boton, int x, int y) {
        boolean dentroDeMargenes = false;
        int engancheX = boton.getLeft() + boton.getWidth() - MARGEN_ENGANCHE;
        int engancheY = boton.getTop();
        Log.i("ÑÑÑEEE", "x = " + x + ", y = " + y);
        Log.i("ÑÑÑEEE", "a = " + engancheX + ", b = " + engancheY);
        if ((x + MARGEN_ENGANCHE) > engancheX && (x - MARGEN_ENGANCHE) < engancheX) {
            if ((y + MARGEN_ENGANCHE) > engancheY && (y - MARGEN_ENGANCHE) < engancheY) {
                dentroDeMargenes = true;
            }
        }
        return dentroDeMargenes;
    }

    private boolean compruebaDistanciasEngancheSuperior(BotonAccion boton, int x, int y) {
        boolean dentroDeMargenes = false;
        int engancheX = boton.getLeft() + ANCHO_BTN_IF - MARGEN_ENGANCHE;
        int engancheY = boton.getTop();
        Log.i("ÑÑÑEEE", " x = " + x + ",  y = " + y);
        Log.i("ÑÑÑEEE", "xS = " + engancheX + ", yS = " + engancheY);
        if ((x + MARGEN_ENGANCHE) > engancheX && (x - MARGEN_ENGANCHE) < engancheX) {
            if ((y + MARGEN_ENGANCHE) > engancheY && (y - MARGEN_ENGANCHE) < engancheY) {
                dentroDeMargenes = true;
            }
        }
        return dentroDeMargenes;
    }

    public void onClick(View view) {
        // para sobreescribir donde se quiera tener onClick
    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }
}
