package es.lucecu.wyspo.service.cvo;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import es.lucecu.wyspo.service.acciones.AccionService;
import es.lucecu.wyspo.service.asyncTasks.BluetoothAsyncTask;
import es.lucecu.wyspo.ui.dialogos.DialogoBluetooth;

public class DispositivoCVO {

    public static final int ESTADO_OFFLINE = 0;
    public static final int ESTADO_CONECTANDO = 1;
    public static final int ESTADO_ONLINE = 2;
    public static final int ESTADO_ERROR = 3;

    private static final UUID mBot_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private BluetoothDevice device;
    private BluetoothAsyncTask bluetoothTask;
    private int estado;

    private AccionService accionService;

    public DispositivoCVO(BluetoothDevice device) {
        this.device = device;
        if(AccionService.hayDispositivosActivos() && device.equals(AccionService.getDispositivoActivo())) {
            this.estado = ESTADO_ONLINE;
        } else {
            this.estado = ESTADO_OFFLINE;
        }
    }

    public String getNombre() {
        return device.getName();
    }

    public String getMac() {
        return device.getAddress();
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;

        // aviso al dialogo de que hay cambios
        Message msg = new Message();
        msg.what = DialogoBluetooth.UPDATE_LISTVIEW_CHANGES;
        DialogoBluetooth.getHandler().sendMessage(msg);
    }

    public byte[] ejecutaAccion(byte data[]) throws IOException{
        byte l[] = limpiaInputStream(); // por si quedó algo de otra acción

StringBuilder sb = new StringBuilder(l.length * 2); //TODO BORRAR
for(byte b: l) //TODO BORRAR
    sb.append(String.format("%02x", b) + "_"); //TODO BORRAR
Log.i("ÑÑÑEEE010", "LIMPIO_" + sb.toString()); //TODO BORRAR

        bluetoothTask.getBluetoothSocket(device).getOutputStream().write(data);
        return leeRespuestaAccion();
    }

    private byte[] leeRespuestaAccion() throws IOException{
        InputStream in = bluetoothTask.getBluetoothSocket(device).getInputStream();
        boolean leer = true;
        boolean primeraBandera = false;
        int lectura;
        List<Integer> bytes = new ArrayList<Integer>();
        while(leer) {
            lectura = in.read();

            // siempre acaba en 0d 0a
            if((primeraBandera && lectura == 0x0a) || lectura == -1) {
                leer = false;
            }
            primeraBandera = lectura == 0x0d;

            bytes.add(lectura);
        }

        byte respuesta[] = new byte[bytes.size()];
        for(int i = 0 ; i < respuesta.length ; i++) {
            respuesta[i] = bytes.get(i).byteValue();
        }

        return respuesta;
    }

    private byte[] limpiaInputStream() throws IOException{
        InputStream in = bluetoothTask.getBluetoothSocket(device).getInputStream();
        List<Integer> bytes = new ArrayList<Integer>();
        while(in.available() > 0) {
            bytes.add(in.read());
        }

        byte respuesta[] = new byte[bytes.size()];
        for(int i = 0 ; i < respuesta.length ; i++) {
            respuesta[i] = bytes.get(i).byteValue();
        }

        return respuesta;
    }

    public void iniciaConexion() {
        setEstado(DispositivoCVO.ESTADO_CONECTANDO);

        // interesa hacerlo en un hilo y no aquí para que la aplicación siga su curso
        bluetoothTask = new BluetoothAsyncTask();
        bluetoothTask.execute(this);
    }

    public void finalizaConexion() {
        if(bluetoothTask != null) {
            bluetoothTask.closeConnection(this);
        }
        bluetoothTask = null;
        setEstado(DispositivoCVO.ESTADO_OFFLINE);
    }

    public boolean contieneDispositivo(BluetoothDevice device) {
        return device.equals(this.device);
    }
}
